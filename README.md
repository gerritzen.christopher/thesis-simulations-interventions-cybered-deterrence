# Simulating Cyberweapons as Deterrence Against Humanitarian Interventions Using Game Theory and Learning Algorithms – Simulations and Data

This repository contains the simulations and data used in my thesis submitted on 25 February 2019 in partial fulfillment of the requirements of the degree of Bachelor of Arts in [International Relations](https://www.hochschule-rhein-waal.de/en/faculties/society-and-economics/degree-programmes/international-relations-ba) at [Hochschule Rhein-Waal / Rhine-Waal University of Applied Sciences](https://www.hochschule-rhein-waal.de/en), [Faculty of Society and Economics](https://www.hochschule-rhein-waal.de/en/faculties/society-and-economics).

Gerritzen, C. (2019) *Simulating Cyberweapons as Deterrence Against Humanitarian Interventions Using Game Theory and Learning Algorithms – Can Small States use Cyberweapons to Deter Interventions by Powerful, Networked States?* Bachelor Thesis. Hochschule Rhein-Waal.

## Repository Contents

+ [**results**](results): Folder containing the simulation results.
  + **standard game static phi0.5 lam0.5**: Simulations results for the simulation sets with *ϕ* = 0.5 and *λ* = 0.5.
  + **standard game series**: Simulation results for simulation super-sets (series of *ϕ* and *λ*).
+ [**gambit**](gambit): Folder containing the files related to Gambit 15.1.1 (McKelvey, McLennan and Turocy, 2014).
  + **CyberedDeterrenceGame_ChancePlayer.gbt**: Gambit 15.1.1 file of the 'Cybered Deterrence Game'.
  + **CyberedDeterrenceGame_ChancePlayer_Qre.csv**: Gambit 15.1.1 Quantal Response Equilibrium (QRE) calculation output for the 'Cybered Deterrence Game'.
+ **Python 3 (Python Software Foundation, no date) files.**
  + [`simulations_seq_phi-lam-static.py`](simulations_seq_phi-lam-static.py): Simulation of 'Sequential Cybered Deterrence Game' for specific *ϕ* and *λ*.
  + [`simulations_seq_phi-lam-series.py`](simulations_seq_phi-lam-series.py): Simulation of 'Sequential Cybered Deterrence Game' for series of *ϕ* and *λ*.
  + [`simulations_matrix_phi-lam-static.py`](simulations_matrix_phi-lam-static.py): Simulation of 'Cybered Deterrence Game' for specific *ϕ* and *λ*.
  + [`simulations_matrix_phi-lam-series.py`](simulations_matrix_phi-lam-series.py): Simulation of 'Cybered Deterrence Game' for series of *ϕ* and *λ*.
  + [`seqgame.py`](seqgame.py): Function for the simulation of 'Sequential Cybered Deterrence Game'.
  + [`matrixgame.py`](matrixgame.py): Function for the simulation of the 'Cybered Deterrence Game'.
  + [`experimetrics.py`](experimetrics.py): Implementation of selected learning algorithms described in Moffatt (2016).
  + [`config.py`](config.py): Configuration file for the simulations.

## References

Fey, M. and Ramsay, K. W. (2011) ‘Uncertainty and Incentives in Crisis Bargaining: Game-Free Analysis of International Conflict’, American Journal of Political Science, 55(1), pp. 149–169. doi: 10.1111/j.1540-5907.2010.00486.x.

McKelvey, R. D., McLennan, A. M. and Turocy, T. L. (2014) Gambit: Software Tools for Game Theory. The Gambit Project. Available at: http://www.gambit-project.org/ (Accessed: 18 December 2018).

Moffatt, P. G. (2016) ‘Chapter 18 Learning Models’, in Experimetrics: econometrics for experimental economics. London New York, NY: Macmillan Education, Palgrave, pp. 419–440.

Python Software Foundation (no date) Python. Python Software Foundation. Available at: https://www.python.org/ (Accessed: 22 January 2019).

Quackenbush, S. L. (2011) ‘Deterrence theory: where do we stand?’, Review of International Studies, 37(02), pp. 741–762. doi: 10.1017/S0260210510000896.

Zagare, F. C. (2004) ‘Reconciling Rationality with Deterrence: A Re-Examination of the Logical Foundations of Deterrence Theory’, Journal of Theoretical Politics, 16(2), pp. 107–141. doi: 10.1177/0951629804041117.
