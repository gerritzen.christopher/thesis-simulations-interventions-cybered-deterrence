# Configuration file for simulations (defaults)

# Enable (True) or disable (False) extended data output
EXTLOG = True
# Number of simulations (can have command line override argv[2])
N = 1000
# Number of periods (can have command line override argv[3])
M_SEQ = 10 # Default number of periods in the sequential game
M_MAT = 100 # Default number of periods in the matrix game
# Sensitivity to attractions (Moffatt, 2016, p. 424)
lam = 1
# Recency parameter (Moffatt, 2016, p. 424)
phi = 1
# If phi = 1, the weighted fictitious play model (wfpm) is equal to standard fictitious play
# If phi = 0, the weighted fictitious play model (wfpm) is equal to Cournot learning model
exp = 0 # Initial experience (wfpm)

# Payoffs
S_SEQ = [2,4,4,2,3,3] # Standard
#S_SEQ = [2,4,4,2,2,2] # Simplified model using average cost instead of random cost
# S_SEQ = [x0,y0,x1,y1,x2,y2]
S_MAT = [2,4,2,4,4,2,3,3] # Standard
#S_MAT = [2,4,2,4,4,2,2,2] # Simplified model using average cost instead of random cost
# S_MAT = [a,b,c,d,e,f,g,h]
# -----------Explanation-----------
# | (a,b) = (2,4) | (c,d) = (2,4) |
# ---------------------------------
# | (e,f) = (4,2) | (g,h) = (3,3) |
# ---------------------------------
# Payoff variation min/max values for player 1 - Cost of conflict of player 1
# Must be 0 if the 'simplified' model is used
C_1_MIN = 0 # Lower boundary of the payoff variation for player 1
C_1_MAX = 2 # Upper boundary of the payoff variation for player 1
# Payoff variation min/max values for player 2 - Cost of conflict of player 2
C_2_MIN = 0 # Lower boundary of the payoff variation for player 2
C_2_MAX = 2 # Upper boundary of the payoff variation for player 2

# Initial attraction values, usually set to 0
attrac_10 = 0 # Player 1: Initial attraction to strategy 1 (w)
attrac_11 = 0 # Player 1: initial attraction to strategy 2 (i)
attrac_20 = 0 # Player 2: Initial attraction to strategy 1 (s)
attrac_21 = 0 # Player 2: Initial attraction to strategy 2 (r)
