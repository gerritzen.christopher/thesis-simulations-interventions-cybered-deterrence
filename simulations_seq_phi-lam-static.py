import math 
import csv
import time
import os
import seqgame
import sys
from config import S_SEQ, N, M_SEQ, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, EXTLOG

TIME = time.strftime("D%Y%m%dT%H%M%S") # Date/time of simulation
LM = str(sys.argv[1]) # Get command line argument 1
print('Learning model: ' + str(sys.argv[1])) # Print used learning model to console

if EXTLOG == True:
    os.makedirs('output/' + TIME + '-' + LM + '/lam' +str(lam)) # Create sub-directory for lambda
    
(n_periods, n_conflict, average_time_to_withdrawal, n_withdrawal, average_time_to_surrender,
 n_surrender, n_timeout, sum_payoff_1, sum_payoff_2, sum_cost_1, sum_cost_2,
 sum_prob_10, sum_prob_11, sum_prob_20, sum_prob_21) = seqgame.scdg(S_SEQ, N, M_SEQ, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, TIME, LM)
