import math
import random

# Function: Strategy selection
def stratselect(prob_i0):
    # Select strategy for player 1
    r = random.uniform(0,1)
    # print(r) # Debug output
    if r <= prob_i0:
        strat_i = 0
    else:
        strat_i = 1
    return strat_i

# Function: Calculation of probability (Moffatt, 2016, p. 424, 18.1)
def probcalc(LAM, attrac_ij, attrac_i0, attrac_i1):
    prob_strat = math.exp(LAM*attrac_ij)/(math.exp(LAM*attrac_i1)+math.exp(LAM*attrac_i0))
    return prob_strat

# Function: Reinforcement learning attraction calculation (Moffatt, 2016, p. 424, 18.2)
def rlcalc(PHI, attrac_prev, payoff):
    attrac_new = PHI*attrac_prev+payoff
    return attrac_new

# Function: Reinforcement learning (Moffatt, 2016, p. 424, 18.2)
def rl(PHI, strat_1, strat_2, payoff_1, payoff_2, attrac_10, attrac_11, attrac_20, attrac_21):
    # Update attractions for player 1
    if strat_1 == 0: # Checks if strategy 1 is selected
        attrac_10 = rlcalc(PHI, attrac_10, payoff_1) # Calls function rlcalc to update the attraction
    elif strat_1 == 1: # Checks if strategy 2 is selected
        attrac_11 = rlcalc(PHI, attrac_11, payoff_1)
    # Update attractions for player 2
    if strat_2 == 0: # Checks if strategy 1 is selected
        attrac_20 = rlcalc(PHI, attrac_20, payoff_2)
    elif strat_2 == 1: # Checks if strategy 2 is selected
        attrac_21 = rlcalc(PHI, attrac_21, payoff_2)
    return attrac_10, attrac_11, attrac_20, attrac_21

# Function: Belief learning attraction calculation (Moffatt, 2016, p. 428, 18.7)
def wfpmcalc(PHI, attrac_prev, payoff, exp_prev, exp_new):
    attrac_new = (PHI*exp_prev*attrac_prev+payoff)/exp_new
    return attrac_new

# Function: Weighted fictitious play model (Moffatt, 2016, p. 428, 18.7)
def wfpm(PHI, payoff_10, payoff_11, payoff_20, payoff_21, attrac_10, attrac_11, attrac_20, attrac_21, exp_prev):
    # Update experience variable
    exp_new = PHI*exp_prev+1
    # Update attractions for player 1
    attrac_10 = wfpmcalc(PHI, attrac_10, payoff_10, exp_prev, exp_new) # Payoff of strategy 1 given the choice of player 2
    attrac_11 = wfpmcalc(PHI, attrac_11, payoff_11, exp_prev, exp_new) # Payoff of strategy 2 given the choice of player 2
    # Update attractions for player 2
    attrac_20 = wfpmcalc(PHI, attrac_20, payoff_20, exp_prev, exp_new) # Payoff of strategy 1 given the choice of player 1
    attrac_21 = wfpmcalc(PHI, attrac_21, payoff_21, exp_prev, exp_new) # Payoff of strategy 2 given the choice of player 1
    return attrac_10, attrac_11, attrac_20, attrac_21, exp_new
