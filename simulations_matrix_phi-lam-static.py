import math 
import csv
import time
import os
import matrixgame
import sys
from config import S_MAT, N, M_MAT, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, EXTLOG

TIME = time.strftime("D%Y%m%dT%H%M%S") # Date/time of simulation
LM = str(sys.argv[1]) # Get command line argument 1
print('Learning model: ' + str(sys.argv[1])) # Print used learning model to console

if EXTLOG == True:
    os.makedirs('output/' + TIME + '-' + LM + '/lam' +str(lam)) # Create sub-directory for lambda
    
    (n_periods, n_ws, n_wr, n_is, n_ir, sum_payoff_1, sum_payoff_2, sum_cost_1, sum_cost_2,
     sum_prob_10, sum_prob_11, sum_prob_20, sum_prob_21) = matrixgame.twobytwo(S_MAT, N, M_MAT, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, TIME, LM)
