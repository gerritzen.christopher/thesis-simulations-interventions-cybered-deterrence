import math
import csv
import time
import os
import seqgame
import sys
from tqdm import trange # Import tqdm progress bar
from config import S_SEQ, N, M_SEQ, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, EXTLOG

TIME = time.strftime("D%Y%m%dT%H%M%S") # Date/time of simulation
LM = str(sys.argv[1]) # Get command line argument 1
print('Learning model: ' + str(sys.argv[1])) # Print used learning model to console

# Create file for summary table
os.makedirs('output/' + TIME + '-' + LM)
summaryTableFile = open('output/' + TIME + '-' + LM + '/' + 'summaryTable-' + TIME + '.csv', 'w', newline='') # Create summary table file
summaryTableWriter = csv.writer(summaryTableFile) # Create data file writer
summaryTableWriter.writerow(['lambda', 'phi', 'n_periods', 'n_conflict',
                             'average time to withdrawal', 'n_withdrawal', 'percentage successful deterrence',
                             'average time to surrender', 'n_surrender', 'percentage successful intervention',
                             'n_timeout', 'percentage timeout',
                             'sum_payoff_1', 'sum_payoff_2', 'average payoff 1', 'average payoff 2','average cost 1', 'average cost 2',
                             'average prob_10 (w)', 'average prob_11 (i)', 'average prob_20 (s)', 'average prob_21 (r)'])

# Run simulations for lambda between 0 and 1
for lam in trange(0,11,desc='Lambda'):
    lam=lam/10 # range() function only supports integers, convert integer to respective float
    
    if EXTLOG == True:
        os.makedirs('output/' + TIME + '-' + LM + '/lam' +str(lam)) # Create sub-directories for each lambda
    
    # Run simulations for phi between 0 and 1
    for phi in trange(0,11,desc='Phi   '):        
        phi=phi/10 # range() function only supports integers, convert integer to respective float
        
        (n_periods, n_conflict, average_time_to_withdrawal, n_withdrawal, average_time_to_surrender,
         n_surrender, n_timeout, sum_payoff_1, sum_payoff_2, sum_cost_1, sum_cost_2,
         sum_prob_10, sum_prob_11, sum_prob_20, sum_prob_21) = seqgame.scdg(S_SEQ, N, M_SEQ, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, TIME, LM)
        
        # Write summary to summary table (1 line for each lam/phi combination)
        summaryTableWriter.writerow([lam, phi, n_periods, n_conflict,
                                     average_time_to_withdrawal, n_withdrawal, n_withdrawal/N*100,
                                     average_time_to_surrender, n_surrender, n_surrender/N*100,
                                     n_timeout, n_timeout/N*100,
                                     sum_payoff_1, sum_payoff_2, sum_payoff_1/n_periods, sum_payoff_2/n_periods, sum_cost_1/n_conflict, sum_cost_2/n_conflict,
                                     sum_prob_10/n_periods, sum_prob_11/n_periods, sum_prob_20/n_periods, sum_prob_21/n_periods])
        
# Close summary table file and print statement of completion
summaryTableFile.close()
print('\nCompleted ' + str(N) + ' simulations for every phi and lambda between 0 and 1 iterated at 0.1')
