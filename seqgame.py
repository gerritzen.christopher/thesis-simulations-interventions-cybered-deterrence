# 'Sequential Cybered Deterrence Game' based on:
# "Rudimentary Asymmetric Deterrence Game", p. 114, Zagare, F. C. (2004) ‘Reconciling Rationality with Deterrence: A Re-Examination of the Logical Foundations of Deterrence Theory’, Journal of Theoretical Politics, 16(2), pp. 107–141. doi: 10.1177/0951629804041117.
# "'Classical' deterrence game", p. 744; "Simple deterrence game with a credible threat", p. 747, Quackenbush, S. L. (2011) ‘Deterrence theory: where do we stand?’, Review of International Studies, 37(02), pp. 741–762. doi: 10.1017/S0260210510000896.
# "Simple Crisis Game", p. 154, Fey, M. and Ramsay, K. W. (2011) ‘Uncertainty and Incentives in Crisis Bargaining: Game-Free Analysis of International Conflict’, American Journal of Political Science, 55(1), pp. 149–169. doi: 10.1111/j.1540-5907.2010.00486.x.

import math
import random
import csv
import experimetrics
from config import EXTLOG

# Function: Simulation for 'Sequential Cybered Deterrence Game'
def scdg(S, N, M, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, TIME, LM):
    # Create output files
    if EXTLOG == True:
        logFile = open('output/' + TIME + '-' + LM + '/lam' + str(lam) + '/' + 'log-' + TIME + '-phi' + str(phi) + '-lam' + str(lam) + '.txt', 'w') # Create log file
        dataFile = open('output/' + TIME + '-' + LM + '/lam' + str(lam) + '/' + 'data-' + TIME + '-phi' + str(phi) + '-lam' + str(lam) + '.csv', 'w', newline='') # Create data file
        dataWriter = csv.writer(dataFile) # Create data file writer
        dataWriter.writerow(['Simulation', 'Period', 'attrac_10 (w)', 'attrac_11 (i)', 'attrac_20 (s)', 'attrac_21 (r)',
                            'prob_10 (w)', 'prob_11 (i)', 'prob_20 (s)', 'prob_21 (r)', 'strat_1', 'strat_1_id', 'strat_2', 'strat_2_id',
                            'payoff_1', 'payoff_2', 'cost_1', 'cost_2'])

    # Initialize variables
    sum_time_to_withdrawal = 0
    sum_time_to_surrender = 0
    sum_payoff_1 = 0
    sum_payoff_2 = 0
    sum_cost_1 = 0
    sum_cost_2 = 0
    sum_prob_10 = 0
    sum_prob_11 = 0
    sum_prob_20 = 0
    sum_prob_21 = 0
    n_withdrawal = 0
    n_surrender = 0
    n_conflict = 0
    n_timeout = 0
    n_periods = 0
    
    # Simulations
    for sim in range(0,N): # Loop that runs N simulations

        from config import exp # Only used for wfpm (experience variable)
        from config import attrac_10, attrac_11, attrac_20, attrac_21 # Resets the attraction values at the beginning of every simulation to the starting values
        end_simulation = False # Initialize end simulation trigger

        for period in range(0,M): # Loop that runs M periods within every simulation
            
            n_periods = n_periods+1 # Count total number of periods within one set of simulations
            
            # Update probabilities
            prob_10 = experimetrics.probcalc(lam, attrac_10, attrac_10, attrac_11) # Player 1: Update probability of strategy 1 (w)
            prob_11 = experimetrics.probcalc(lam, attrac_11, attrac_10, attrac_11) # Player 1: Update probability of strategy 2 (i)
            prob_20 = experimetrics.probcalc(lam, attrac_20, attrac_20, attrac_21) # Player 2: Update probability of strategy 1 (r)
            prob_21 = experimetrics.probcalc(lam, attrac_21, attrac_20, attrac_21) # Player 2: Update probability of strategy 2 (s)
            # Save sum of probabilities
            sum_prob_10 = sum_prob_10+prob_10
            sum_prob_11 = sum_prob_11+prob_11
            sum_prob_20 = sum_prob_20+prob_20
            sum_prob_21 = sum_prob_21+prob_21

            # Select strategy of player 1
            strat_1 = experimetrics.stratselect(prob_10)
            
            # Player 1 decides not to intervene
            if strat_1 == 0: # Determine payoffs for case player 1 does not intervene
                payoff_1 = S[0]
                payoff_2 = S[1]
                sum_time_to_withdrawal = sum_time_to_withdrawal+period+1 # +1 to count the initial period as first instead of null period
                n_withdrawal = n_withdrawal+1 # Count simulations that end with withdrawal of player 1
                # Print output
                if EXTLOG == True:
                    print('Simulation ' + str(sim) + '/' + str(period) + ': Player 1 withdraws' + '\n' +
                          '→ Player 2 wins: Successful deterrence', file=logFile) # Log output
                    dataWriter.writerow([sim, period, attrac_10, attrac_11, attrac_20, attrac_21, prob_10, prob_11, prob_20, prob_21, strat_1, 'w', '', '', payoff_1, payoff_2])
                # Trigger end simulation
                end_simulation = True
            
            # Player 1 decides to intervene
            elif strat_1 == 1: # Determine payoffs for case player 1 intervenes based on decision of player 2
                if EXTLOG == True:
                    print('Simulation ' + str(sim) + '/' + str(period) + ': Player 1 intervenes', file=logFile) # Log output
                
                if period == M-1:
                    strat_2 = 0
                    n_timeout = n_timeout+1 # Count simulations that end with surrender of player 2 after timeout                    
                    if EXTLOG == True:
                        print('Player 2 has lost the war', file=logFile)
                else:
                    strat_2 = experimetrics.stratselect(prob_20)
                
                # Player 2 decides to surrender
                if strat_2 == 0:
                    payoff_1 = S[2]
                    payoff_2 = S[3]
                    sum_time_to_surrender = sum_time_to_surrender+period+1
                    n_surrender = n_surrender+1 # Count simulations that end with surrender of player 2
                    # Print output
                    if EXTLOG == True:
                        dataWriter.writerow([sim, period, attrac_10, attrac_11, attrac_20, attrac_21, prob_10, prob_11, prob_20, prob_21, strat_1, 'i', strat_2, 's', payoff_1, payoff_2])
                        print('Simulation ' + str(sim) + '/' + str(period) + ': Player 2 surrenders' + '\n' +
                              '→ Player 1 wins: Successful intervention', file=logFile) # Log output
                    # Trigger end simulation
                    end_simulation = True                    
                
                # Player 2 decides to retaliate
                elif strat_2 == 1:
                    cost_1 = random.randint(C_1_MIN,C_1_MAX) # Cost of conflict player 1
                    cost_2 = random.randint(C_2_MIN,C_2_MAX) # Cost of conflict player 2
                    payoff_1 = S[4]-cost_1
                    payoff_2 = S[5]-cost_2
                    n_conflict = n_conflict+1 # Count periods of conflict
                    sum_cost_1 = sum_cost_1+cost_1
                    sum_cost_2 = sum_cost_2+cost_2
                    # Print output
                    if EXTLOG == True:
                        dataWriter.writerow([sim, period, attrac_10, attrac_11, attrac_20, attrac_21, prob_10, prob_11, prob_20, prob_21, strat_1, 'i', strat_2, 'r', payoff_1, payoff_2, cost_1, cost_2])
                        print('Simulation ' + str(sim) + '/' + str(period) + ': Player 2 retaliates', file=logFile) # Log output
                        print('PAYOFF VARIATION: cost_1 = ' + str(cost_1) + '; cost_2 = ' + str(cost_2), file=logFile) # Log output
                    
            # Update payoff sum
            sum_payoff_1 = sum_payoff_1+payoff_1
            sum_payoff_2 = sum_payoff_2+payoff_2
            
            # End simulation if triggered
            if end_simulation == True:
                break
            else:
                if LM == 'rl':
                    # Update attractions
                    attrac_10, attrac_11, attrac_20, attrac_21 = experimetrics.rl(phi, strat_1, strat_2, payoff_1, payoff_2, attrac_10, attrac_11, attrac_20, attrac_21)
                elif LM == 'wfpm':
                    # Payoffs of conflict and hypothetical payoffs of alternatives
                    payoff_10 = S[0]
                    payoff_11 = payoff_1
                    payoff_20 = S[3]
                    payoff_21 = payoff_2
                    # Update attractions
                    attrac_10, attrac_11, attrac_20, attrac_21, exp = experimetrics.wfpm(phi, payoff_10, payoff_11, payoff_20, payoff_21, attrac_10, attrac_11, attrac_20, attrac_21, exp)
    
    if EXTLOG == True:
        logFile.close()
        dataFile.close()
    
    average_time_to_withdrawal = sum_time_to_withdrawal/n_withdrawal
    average_time_to_surrender = sum_time_to_surrender/n_surrender
    
    return n_periods, n_conflict, average_time_to_withdrawal, n_withdrawal, average_time_to_surrender, n_surrender, n_timeout, sum_payoff_1, sum_payoff_2, sum_cost_1, sum_cost_2, sum_prob_10, sum_prob_11, sum_prob_20, sum_prob_21
