import math
import random
import csv
import experimetrics
from config import EXTLOG

# Function: Simulation for 2x2 matrix game
def twobytwo(S, N, M, C_1_MIN, C_1_MAX, C_2_MIN, C_2_MAX, lam, phi, TIME, LM):
    # Create output files
    if EXTLOG == True:
        dataFile = open('output/' + TIME + '-' + LM + '/lam' + str(lam) + '/' + 'data-' + TIME + '-phi' + str(phi) + '-lam' + str(lam) + '.csv', 'w', newline='') # Create data file
        dataWriter = csv.writer(dataFile) # Create data file writer
        dataWriter.writerow(['Simulation', 'Period', 'attrac_10', 'attrac_11', 'attrac_20', 'attrac_21',
                            'prob_10', 'prob_11', 'prob_20', 'prob_21', 'strat_1', 'strat_2',
                            'payoff_1', 'payoff_2', 'cost_1', 'cost_2'])

    # Initialize variables
    sum_payoff_1 = 0
    sum_payoff_2 = 0
    sum_cost_1 = 0
    sum_cost_2 = 0
    sum_prob_10 = 0
    sum_prob_11 = 0
    sum_prob_20 = 0
    sum_prob_21 = 0
    n_00 = 0
    n_01 = 0
    n_10 = 0
    n_11 = 0
    n_periods = 0
    
    # Simulations
    for sim in range(0,N): # Loop that runs N simulations

        from config import exp # Only used for wfpm (experience variable)
        from config import attrac_10, attrac_11, attrac_20, attrac_21 # Resets the attraction values at the beginning of every simulation to the starting values

        for period in range(0,M): # Loop that runs M periods within every simulation
            
            n_periods = n_periods+1
            
            # Update probabilities
            prob_10 = experimetrics.probcalc(lam, attrac_10, attrac_10, attrac_11) # Player 1: Update probability of strategy 1
            prob_11 = experimetrics.probcalc(lam, attrac_11, attrac_10, attrac_11) # Player 1: Update probability of strategy 2
            prob_20 = experimetrics.probcalc(lam, attrac_20, attrac_20, attrac_21) # Player 2: Update probability of strategy 1
            prob_21 = experimetrics.probcalc(lam, attrac_21, attrac_20, attrac_21) # Player 2: Update probability of strategy 2
            # Save sum of probabilities
            sum_prob_10 = sum_prob_10+prob_10
            sum_prob_11 = sum_prob_11+prob_11
            sum_prob_20 = sum_prob_20+prob_20
            sum_prob_21 = sum_prob_21+prob_21
            # Determine cost variables
            cost_1 = random.randint(C_1_MIN, C_1_MAX)
            cost_2 = random.randint(C_2_MIN, C_2_MAX)
            sum_cost_1 = sum_cost_1+cost_1
            sum_cost_2 = sum_cost_2+cost_2

            # Select strategy of player 1
            strat_1 = experimetrics.stratselect(prob_10)
            # Select strategy of player 2
            strat_2 = experimetrics.stratselect(prob_20)
            
            # Determine payoffs
            if strat_1 == 0 and strat_2 == 0: # Strategy 1 / Strategy 1
                n_00 = n_00+1
                payoff_1 = S[0]
                payoff_2 = S[1]
            elif strat_1 == 0 and strat_2 == 1: # Strategy 1 / Strategy 2
                n_01 = n_01+1
                payoff_1 = S[2]
                payoff_2 = S[3]
            elif strat_1 == 1 and strat_2 == 0: # Strategy 2 / Strategy 1
                n_10 = n_10+1
                payoff_1 = S[4]
                payoff_2 = S[5]
            elif strat_1 == 1 and strat_2 == 1: # Strategy 2 / Strategy 2
                n_11 = n_11+1
                payoff_1 = S[6]-cost_1
                payoff_2 = S[7]-cost_2
                
            # Update payoff sum
            sum_payoff_1 = sum_payoff_1+payoff_1
            sum_payoff_2 = sum_payoff_2+payoff_2
            # Print output to .csv file and save data
            if EXTLOG == True:
                dataWriter.writerow([sim, period, attrac_10, attrac_11, attrac_20, attrac_21, prob_10, prob_11, prob_20, prob_21, strat_1, strat_2, payoff_1, payoff_2])
            
            # Update attractions depending on the selected learning model
            if LM == 'rl':
                attrac_10, attrac_11, attrac_20, attrac_21 = experimetrics.rl(phi, strat_1, strat_2, payoff_1, payoff_2, attrac_10, attrac_11, attrac_20, attrac_21)
            elif LM == 'wfpm':
                # Potential payoffs of player 1
                if strat_2 == 0:
                    payoff_10 = S[0]
                    payoff_11 = S[4]
                elif strat_2 == 1:
                    payoff_10 = S[2]
                    payoff_11 = S[6]-cost_1
                # Potential payoffs of player 2
                if strat_1 == 0:
                    payoff_20 = S[1]
                    payoff_21 = S[3]
                elif strat_1 == 1:
                    payoff_20 = S[5]
                    payoff_21 = S[7]-cost_2
                # Update attractions
                attrac_10, attrac_11, attrac_20, attrac_21, exp = experimetrics.wfpm(phi, payoff_10, payoff_11, payoff_20, payoff_21, attrac_10, attrac_11, attrac_20, attrac_21, exp)

    if EXTLOG == True:
        dataFile.close()
    
    return n_periods, n_00, n_01, n_10, n_11, sum_payoff_1, sum_payoff_2, sum_cost_1, sum_cost_2, sum_prob_10, sum_prob_11, sum_prob_20, sum_prob_21
